package com.agrzybow.companyapp.android.postmanagement.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Klasa przechowująca dane dotyczące wpisu na tablicy ogłoszeń dostępnej w aplikacji mobilnej
 */
@Data
@NoArgsConstructor
@Entity
@Table(name = "Post")
public class PostEntity {
    public static final String ID_FIELD_NAME = "id";
    public static final String TITLE_FIELD_NAME = "title";
    public static final String POST_TEXT_FIELD_NAME = "postText";
    public static final String READ_BY_CLIENT_FIELD_NAME = "readByClient";
    public static final String DATE_FIELD_NAME = "modifyDate";

    @Id
    @Column(name = ID_FIELD_NAME)
    private Integer id;

    @NotNull
    @Size(min = 1)
    @Column(name = TITLE_FIELD_NAME, nullable = false)
    private String title;

    @NotNull
    @Size(min = 1)
    @Column(name = POST_TEXT_FIELD_NAME, nullable = false, columnDefinition = "TEXT")
    private String postText;

    @NotNull
    @Column(name = READ_BY_CLIENT_FIELD_NAME, nullable = false)
    private Boolean readByClient;

    @NotNull
    @Min(1)
    @Column(name = DATE_FIELD_NAME, nullable = false)
    private Long modifyDate;
}
