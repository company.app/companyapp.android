package com.agrzybow.companyapp.android.chatmanagement.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import androidx.preference.PreferenceManager;
import co.intentservice.chatui.models.ChatMessage;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.chatmanagement.model.ChatMessageEntity;
import com.agrzybow.companyapp.android.chatmanagement.model.repository.ChatMessageEntityRepository;
import com.agrzybow.companyapp.android.chatmanagement.service.ChatMessageService;
import com.agrzybow.companyapp.android.general.model.UserEntity;

import co.intentservice.chatui.ChatView;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.google.gson.Gson;

public class ChatActivity extends AppCompatActivity {
    private static final int messageMaxLength = 255;

    public static final String CONTACT_USER_OBJECT = "com.agrzybow.companyapp.android.chat.activity.contact.user.object";
    public static final String RECEIVED_MESSAGE = "com.agrzybow.companyapp.android.chat.activity.message.received";
    public static final String MESSAGE_BUNDLE = "com.agrzybow.companyapp.android.chat.activity.message.bundle";

    private UserEntity contact;
    private UserEntity client;

    private ChatMessageService chatMessageService;
    private ChatMessageEntityRepository chatMessageEntityRepository;

    private ChatView mChatView;
    private BroadcastReceiver mBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            contact = (UserEntity) bundle.getSerializable(CONTACT_USER_OBJECT);
        }
        client = new Gson().fromJson(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString(UserService.CLIENT_ENTITY, null), UserEntity.class);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(contact.getName());
            actionBar.setElevation(0);
        }

        ((TextView) findViewById(R.id.chat_contact_email)).setText(contact.getEmail());
        ((TextView) findViewById(R.id.chat_contact_phone_number)).setText(contact.getPhoneNumber());

        chatMessageService = new ChatMessageService(getApplicationContext());
        mChatView = findViewById(R.id.chat_view);
        mChatView.getInputEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(messageMaxLength)});
        mChatView.getInputEditText().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length() == messageMaxLength) {
                    mChatView.getInputEditText().setError(getResources().getString(R.string.chat_message_max_length_reached));
                } else {
                    mChatView.getInputEditText().setError(null);
                }
            }
        });
        mChatView.setOnSentMessageListener(chatMessage -> {
            chatMessageService.saveEntity(ChatMessageEntity.builder()
                    .messageText(chatMessage.getMessage())
                    .userEntityReceiver(contact)
                    .modifyDate(System.currentTimeMillis())
                    .modifyBy(client)
                    .build(), response -> {});
            return true;
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        chatMessageEntityRepository = new ChatMessageEntityRepository(getApplicationContext());
        chatMessageService.getAllEntitiesForContact(contact, chatMessageEntities -> {
            chatMessageEntities.forEach(message -> {
                message.setRead(true);
                if (message.getUserEntityReceiver().getUsername().equals(contact.getUsername())) {
                    mChatView.addMessage(new ChatMessage(message.getMessageText(), message.getModifyDate(), ChatMessage.Type.SENT));
                } else {
                    mChatView.addMessage(new ChatMessage(message.getMessageText(), message.getModifyDate(), ChatMessage.Type.RECEIVED));
                }
            });
            chatMessageEntityRepository.createOrUpdateEntities(chatMessageEntities);
        });

        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (RECEIVED_MESSAGE.equals(intent.getAction())) {
                    Bundle receivedMessageBundle = intent.getExtras();
                    if (receivedMessageBundle != null) {
                        ChatMessageEntity receivedMessage = (ChatMessageEntity) receivedMessageBundle.getSerializable(MESSAGE_BUNDLE);
                        if (receivedMessage != null && receivedMessage.getModifyBy().getUsername().equals(contact.getUsername())) {
                            ChatMessage uiReceivedChatMessage = new ChatMessage(receivedMessage.getMessageText(), receivedMessage.getModifyDate(), ChatMessage.Type.RECEIVED);
                            mChatView.addMessage(uiReceivedChatMessage);
                        }
                    }
                }
            }
        };

        registerReceiver(mBroadcastReceiver, new IntentFilter(RECEIVED_MESSAGE));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
        }
        return super.onOptionsItemSelected(item);
    }
}

