package com.agrzybow.companyapp.android.quizmanagement.model.dto;

import lombok.Data;

@Data
public class QuizAnswerDTO {
    private Integer id;
    private String answerText;
    private Boolean isCorrect;
    private QuizQuestionDTO question;
}
