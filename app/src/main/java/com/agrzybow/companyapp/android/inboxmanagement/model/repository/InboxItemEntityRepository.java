package com.agrzybow.companyapp.android.inboxmanagement.model.repository;

import android.content.Context;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.agrzybow.companyapp.android.inboxmanagement.model.InboxItemEntity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class InboxItemEntityRepository {
    private Dao<InboxItemEntity, Integer> dao;
    
    public InboxItemEntityRepository(Context context) {
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(InboxItemEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public InboxItemEntity getNewestEntity() throws SQLException {
        return dao.queryBuilder().orderBy(InboxItemEntity.DATE_FIELD_NAME, false).queryForFirst();
    }

    public List<InboxItemEntity> getAllEntities() throws SQLException {
        return dao.queryBuilder().orderBy(InboxItemEntity.DATE_FIELD_NAME, false).query();
    }

    public InboxItemEntity getEntityById(Integer id) throws SQLException {
        return dao.queryForId(id);
    }

    public void createOrUpdateEntity(InboxItemEntity inboxItemEntity) throws SQLException {
        dao.createOrUpdate(inboxItemEntity);
    }

    public void createOrUpdateEntities(List<InboxItemEntity> inboxItemEntities) throws SQLException {
        for (InboxItemEntity inboxItemEntity : inboxItemEntities) {
            createOrUpdateEntity(inboxItemEntity);
        }
    }
}
