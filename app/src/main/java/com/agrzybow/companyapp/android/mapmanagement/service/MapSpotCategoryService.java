package com.agrzybow.companyapp.android.mapmanagement.service;

import android.content.Context;
import android.widget.Toast;
import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.general.service.RetrofitClientService;
import com.agrzybow.companyapp.android.general.service.UserService;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotCategoryEntity;
import com.agrzybow.companyapp.android.mapmanagement.model.api.MapSpotCategoryAPI;
import com.agrzybow.companyapp.android.mapmanagement.model.repository.MapSpotCategoryEntityRepository;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MapSpotCategoryService {
    private MapSpotCategoryAPI mapSpotCategoryAPI;
    private MapSpotCategoryEntityRepository mapSpotCategoryEntityRepository;

    private Context context;
    private List<Disposable> disposableList = new ArrayList<>();

    public MapSpotCategoryService(Context context) {
        this.context = context;
        mapSpotCategoryAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(MapSpotCategoryAPI.class);
        mapSpotCategoryEntityRepository = new MapSpotCategoryEntityRepository(context);
    }

    public void getMapSpotCategoryList(Consumer<List<MapSpotCategoryEntity>> consumer) {
        Long lastEntityDate = null;
        try {
            MapSpotCategoryEntity newestMapSpotCategoryEntity = mapSpotCategoryEntityRepository.getNewestEntity();
            if (newestMapSpotCategoryEntity != null) {
                lastEntityDate = newestMapSpotCategoryEntity.getModifyDate();
            }
        } catch (SQLException ignored) {
        }

        disposableList.add(mapSpotCategoryAPI.getAllEntities(lastEntityDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(mapSpotCategoryEntities -> {
                    mapSpotCategoryEntityRepository.createOrUpdateEntities(mapSpotCategoryEntities);
                    consumer.accept(mapSpotCategoryEntityRepository.getAllEntities());
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                    consumer.accept(mapSpotCategoryEntityRepository.getAllEntities());
                }));
    }
}
