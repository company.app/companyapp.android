package com.agrzybow.companyapp.android.formmanagement.service;

import android.content.Context;
import android.widget.Toast;
import androidx.preference.PreferenceManager;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.formmanagement.model.FormEntity;
import com.agrzybow.companyapp.android.formmanagement.model.api.FormAPI;
import com.agrzybow.companyapp.android.formmanagement.model.repository.FormEntityRepository;
import com.agrzybow.companyapp.android.general.service.RetrofitClientService;
import com.agrzybow.companyapp.android.general.service.UserService;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class FormService {
    private FormAPI formAPI;
    private FormEntityRepository formEntityRepository;

    private List<Disposable> disposableList = new ArrayList<>();

    public FormService(Context context) {
        formAPI = RetrofitClientService.getInstance(PreferenceManager.getDefaultSharedPreferences(context).getString(UserService.CLIENT_CREDENTIALS, null)).getService(FormAPI.class);
        formEntityRepository = new FormEntityRepository(context);
    }

    public void getFormList(Consumer<List<FormEntity>> consumer, Context context) {
        Long lastEntityDate = null;
        try {
            FormEntity newestFormEntity = formEntityRepository.getNewestEntity();
            if(newestFormEntity != null) {
                lastEntityDate = newestFormEntity.getModifyDate();
            }
        } catch (SQLException ignored) {}

        disposableList.add(formAPI.getAllEntities(lastEntityDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(formEntities -> {
                    formEntityRepository.createOrUpdateEntities(formEntities);
                    consumer.accept(formEntityRepository.getAllEntities());
                }, ex -> {
                    ex.printStackTrace();
                    Toast.makeText(context, R.string.error_server_connecting, Toast.LENGTH_LONG).show();
                    consumer.accept(formEntityRepository.getAllEntities());
                }));
    }
}
