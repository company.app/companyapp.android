package com.agrzybow.companyapp.android.inboxmanagement.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.inboxmanagement.model.InboxItemEntity;
import com.agrzybow.companyapp.android.inboxmanagement.service.InboxService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InboxAdapter extends RecyclerView.Adapter<InboxHolder> {
    private List<InboxItemEntity> entityList = new ArrayList<>();
    private List<InboxItemEntity> allEntitiesCopy;

    private InboxService inboxService;

    private Context context;

    public InboxAdapter(Context context) {
        this.context = context;
        this.inboxService = new InboxService(context);
        inboxService.getInboxItemList(inboxItemEntities -> {
            entityList = inboxItemEntities;
            allEntitiesCopy = new ArrayList<>(inboxItemEntities);
            notifyDataSetChanged();
        }, context);
    }

    @NonNull
    @Override
    public InboxHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new InboxHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.inbox_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull InboxHolder holder, int position) {
        InboxItemEntity inboxItemEntity = entityList.get(position);
        holder.bindInboxItem(inboxItemEntity, button -> {
            inboxService.setAsRead(inboxItemEntity, response -> {
                entityList.set(position, response);
                allEntitiesCopy.set(allEntitiesCopy.indexOf(inboxItemEntity), response);
                notifyDataSetChanged();
            });
        });
    }

    @Override
    public int getItemCount() {
        return entityList.size();
    }

    public void filter(boolean readCondition) {
        entityList = allEntitiesCopy.stream().filter(inboxItemEntity -> inboxItemEntity.getReadByReceiver().equals(readCondition)).collect(Collectors.toList());
        notifyDataSetChanged();
    }

    public void clearFilter() {
        entityList = allEntitiesCopy;
        notifyDataSetChanged();
    }
}
