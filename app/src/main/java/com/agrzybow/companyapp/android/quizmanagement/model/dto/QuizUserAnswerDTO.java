package com.agrzybow.companyapp.android.quizmanagement.model.dto;

import com.agrzybow.companyapp.android.general.model.UserEntity;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class QuizUserAnswerDTO {
    private Integer id;
    private QuizAnswerDTO answer;
    private QuizQuestionDTO question;
    private UserEntity modifyBy;
    private Long modifyDate;
}
