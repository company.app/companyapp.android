package com.agrzybow.companyapp.android.quizmanagement.model.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuizDTO {
    private QuizCategoryDTO category;
    private QuizQuestionDTO question;
    private List<QuizAnswerDTO> answers;
}
