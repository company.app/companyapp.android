package com.agrzybow.companyapp.android.mapmanagement.model.repository;

import android.content.Context;
import com.agrzybow.companyapp.android.general.util.DatabaseHelper;
import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotCategoryEntity;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

public class MapSpotCategoryEntityRepository {
    private Dao<MapSpotCategoryEntity, Integer> dao;

    public MapSpotCategoryEntityRepository(Context context) {
        try {
            this.dao = DatabaseHelper.getInstance(context).getDao(MapSpotCategoryEntity.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public MapSpotCategoryEntity getNewestEntity() throws SQLException {
        return dao.queryBuilder().orderBy(MapSpotCategoryEntity.DATE_FIELD_NAME, false).queryForFirst();
    }

    public List<MapSpotCategoryEntity> getAllEntities() throws SQLException {
        return dao.queryBuilder().orderBy(MapSpotCategoryEntity.DATE_FIELD_NAME, false).query();
    }

    public MapSpotCategoryEntity getEntityById(Integer id) throws SQLException {
        return dao.queryForId(id);
    }

    public void createOrUpdateEntity(MapSpotCategoryEntity mapSpotCategoryEntity) throws SQLException {
        dao.createOrUpdate(mapSpotCategoryEntity);
    }

    public void createOrUpdateEntities(List<MapSpotCategoryEntity> formEntities) throws SQLException {
        for (MapSpotCategoryEntity mapSpotCategoryEntity : formEntities) {
            createOrUpdateEntity(mapSpotCategoryEntity);
        }
    }
}
