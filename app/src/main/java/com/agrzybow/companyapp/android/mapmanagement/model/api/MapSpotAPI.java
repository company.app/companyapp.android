package com.agrzybow.companyapp.android.mapmanagement.model.api;

import com.agrzybow.companyapp.android.mapmanagement.model.MapSpotEntity;
import io.reactivex.Observable;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

import java.util.List;

public interface MapSpotAPI {
    @GET("map-spot/display")
    Observable<List<MapSpotEntity>> getAllEntities(@Query("newerThan") Long newerThan);
    @POST("map-spot/add")
    Observable<MapSpotEntity> saveEntity(@Query("username") String username, @Body MapSpotEntity mapSpotEntity);
}
