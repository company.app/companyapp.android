package com.agrzybow.companyapp.android.postmanagement.adapter;

import android.view.View;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.postmanagement.model.PostEntity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PostHolder extends RecyclerView.ViewHolder {
    private TextView titleTextView;
    private TextView dateTextView;
    private TextView timeTextView;
    private TextView postTextView;

    public PostHolder(View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.post_title);
        dateTextView = itemView.findViewById(R.id.post_added_date);
        timeTextView = itemView.findViewById(R.id.post_added_time);
        postTextView = itemView.findViewById(R.id.post_text);
    }

    public void bindPost(PostEntity postEntity) {
        Date date = new Date(postEntity.getModifyDate());

        titleTextView.setText(postEntity.getTitle());
        dateTextView.setText(SimpleDateFormat.getDateInstance().format(date));
        timeTextView.setText(SimpleDateFormat.getTimeInstance().format(date));
        postTextView.setText(postEntity.getPostText());
    }
}