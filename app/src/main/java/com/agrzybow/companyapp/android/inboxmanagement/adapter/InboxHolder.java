package com.agrzybow.companyapp.android.inboxmanagement.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.inboxmanagement.model.InboxItemEntity;

import java.text.SimpleDateFormat;
import java.util.Date;

public class InboxHolder extends RecyclerView.ViewHolder {
    private TextView titleTextView;
    private TextView dateTextView;
    private TextView timeTextView;
    private TextView addedByTextView;
    private TextView textTextView;
    private Button confirmButton;

    public InboxHolder(@NonNull View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.item_title);
        dateTextView = itemView.findViewById(R.id.item_added_date);
        timeTextView = itemView.findViewById(R.id.item_added_time);
        addedByTextView = itemView.findViewById(R.id.item_added_by);
        textTextView = itemView.findViewById(R.id.item_text);
        confirmButton = itemView.findViewById(R.id.item_confirm_button);
    }

    public void bindInboxItem(InboxItemEntity inboxItemEntity, View.OnClickListener confirmButtonListener) {
        Date date = new Date(inboxItemEntity.getModifyDate());
        titleTextView.setText(inboxItemEntity.getTitle());
        dateTextView.setText(SimpleDateFormat.getDateInstance().format(date));
        timeTextView.setText(SimpleDateFormat.getTimeInstance().format(date));
        addedByTextView.setText(inboxItemEntity.getModifyBy().getUsername());
        textTextView.setText(inboxItemEntity.getMessageText());
        if(inboxItemEntity.getReadByReceiver()) {
            confirmButton.setVisibility(View.GONE);
        } else {
            confirmButton.setVisibility(View.VISIBLE);
            confirmButton.setOnClickListener(confirmButtonListener);
        }
    }
}
