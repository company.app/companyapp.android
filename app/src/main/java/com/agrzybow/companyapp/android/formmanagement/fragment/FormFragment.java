package com.agrzybow.companyapp.android.formmanagement.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.agrzybow.R;
import com.agrzybow.companyapp.android.formmanagement.adapter.FormAdapter;

public class FormFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_forms, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        RecyclerView formRecyclerView = getActivity().findViewById(R.id.form_recycler_view);
        formRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        formRecyclerView.setAdapter(new FormAdapter(getContext()));
    }
}
