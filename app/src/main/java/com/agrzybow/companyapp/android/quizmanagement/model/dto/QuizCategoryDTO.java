package com.agrzybow.companyapp.android.quizmanagement.model.dto;

import lombok.Data;

@Data
public class QuizCategoryDTO {
    private Integer id;
    private String categoryText;
}
