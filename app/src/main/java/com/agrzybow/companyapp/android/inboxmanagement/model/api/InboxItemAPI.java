package com.agrzybow.companyapp.android.inboxmanagement.model.api;

import com.agrzybow.companyapp.android.inboxmanagement.model.InboxItemEntity;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

import java.util.List;

public interface InboxItemAPI {
    @GET("inbox-item/{username}/display")
    Observable<List<InboxItemEntity>> getAllEntities(@Path("username") String username, @Query("newerThan") Long newerThan);
    @POST("inbox-item/{username}/display/{id}/mark-as-read")
    Observable<InboxItemEntity> markAsRead(@Path("username") String username, @Path("id") Integer id);
}
